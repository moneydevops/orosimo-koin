class CreateSuggestedPosts < ActiveRecord::Migration[5.0]
  def change
    create_table :suggested_posts do |t|
      t.text :body
      t.integer :mathima_id

      t.timestamps
    end
  end
end
