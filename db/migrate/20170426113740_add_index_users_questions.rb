class AddIndexUsersQuestions < ActiveRecord::Migration[5.0]
  def change
    add_index :questions, [:user_id, :created_at]
  end
end
