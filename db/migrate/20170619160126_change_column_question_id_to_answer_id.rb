class ChangeColumnQuestionIdToAnswerId < ActiveRecord::Migration[5.0]
  def change
    remove_column :question_comments, :question_id
  end
end
