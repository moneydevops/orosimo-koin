class ChangeDefaultRoleForUsers < ActiveRecord::Migration[5.0]
  def change
    change_column :users, :roles, :string , :default => "user"
  end
end
