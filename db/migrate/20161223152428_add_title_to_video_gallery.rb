class AddTitleToVideoGallery < ActiveRecord::Migration[5.0]
  def change
    add_column :video_galleries, :title, :string
  end
end
