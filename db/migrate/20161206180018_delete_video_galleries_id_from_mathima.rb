class DeleteVideoGalleriesIdFromMathima < ActiveRecord::Migration[5.0]
  def change
    remove_column :mathimas, :video_gallery_id
  end
end
