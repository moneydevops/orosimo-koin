class AddMathimaIdToVideoGallery < ActiveRecord::Migration[5.0]
  def change
    add_column :video_galleries, :mathima_id, :integer
  end
end
