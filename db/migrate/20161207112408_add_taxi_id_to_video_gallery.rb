class AddTaxiIdToVideoGallery < ActiveRecord::Migration[5.0]
  def change
    add_column :video_galleries, :taxi_id, :integer
  end
end
