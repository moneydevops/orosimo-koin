class AddColumnPriorityToVideoGalleries < ActiveRecord::Migration[5.0]
  def change
    add_column :video_galleries, :priority, :integer
  end
end
