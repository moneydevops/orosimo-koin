class AddVideoGalleriesIdToMathima < ActiveRecord::Migration[5.0]
  def change
    add_column :mathimas, :video_gallery_id, :integer
  end
end
