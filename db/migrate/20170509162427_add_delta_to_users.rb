class AddDeltaToUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :delta, :boolean, :default => true, :null=>false
  end
end
