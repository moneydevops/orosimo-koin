class CreateQuestions < ActiveRecord::Migration[5.0]
  def change
    create_table :questions do |t|
      t.string :body
      t.integer :user_id
      t.integer :video_gallery_id

      t.timestamps
    end
  end
end
