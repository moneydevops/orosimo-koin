class AddUserIdToComments < ActiveRecord::Migration[5.0]
  def change
    add_column :question_comments, :user_id, :integer
  end
end
