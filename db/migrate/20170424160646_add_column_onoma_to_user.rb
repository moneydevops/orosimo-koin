class AddColumnOnomaToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :onoma, :string
    add_column :users, :eponimo, :string
  end
end
