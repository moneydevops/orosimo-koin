class CreateQuestionComments < ActiveRecord::Migration[5.0]
  def change
    create_table :question_comments do |t|
      t.integer :question_id
      t.string :body

      t.timestamps
    end
  end
end
