class AddColumnAnswerId < ActiveRecord::Migration[5.0]
  def change
    add_column :question_comments, :answer_id, :integer
  end
end
