class CreateMathimas < ActiveRecord::Migration[5.0]
  def change
    create_table :mathimas do |t|
      t.string :mathima

      t.timestamps
    end
  end
end
