require 'test_helper'

class MathimasControllerTest < ActionDispatch::IntegrationTest
  setup do
    @mathima = mathimas(:one)
  end

  test "should get index" do
    get mathimas_url
    assert_response :success
  end

  test "should get new" do
    get new_mathima_url
    assert_response :success
  end

  test "should create mathima" do
    assert_difference('Mathima.count') do
      post mathimas_url, params: { mathima: { mathima: @mathima.mathima } }
    end

    assert_redirected_to mathima_url(Mathima.last)
  end

  test "should show mathima" do
    get mathima_url(@mathima)
    assert_response :success
  end

  test "should get edit" do
    get edit_mathima_url(@mathima)
    assert_response :success
  end

  test "should update mathima" do
    patch mathima_url(@mathima), params: { mathima: { mathima: @mathima.mathima } }
    assert_redirected_to mathima_url(@mathima)
  end

  test "should destroy mathima" do
    assert_difference('Mathima.count', -1) do
      delete mathima_url(@mathima)
    end

    assert_redirected_to mathimas_url
  end
end
