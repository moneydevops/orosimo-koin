ThinkingSphinx::Index.define :user, :with => :active_record, :delta =>true do
  #fields
  indexes onoma, :sortable => true
  indexes eponimo
  indexes email
  indexes roles

  #attributed
  has created_at, updated_at

  where sanitize_sql(["approved", true])
end
