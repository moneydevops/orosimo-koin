class MathimasController < ApplicationController
  before_action :collect_all
  before_action :authenticate_user!, except: [:show]
  load_and_authorize_resource

  # GET /mathimas
  # GET /mathimas.json
  def index
  end

  # GET /mathimas/1
  # GET /mathimas/1.json
  def show
  end

  # GET /mathimas/new
  def new
  end

  # GET /mathimas/1/edit
  def edit
  end

  # POST /mathimas
  # POST /mathimas.json
  def create
    @mathima = Mathima.new(mathima_params)

    respond_to do |format|
      if @mathima.save
        format.html { redirect_to @mathima, notice: 'Mathima was successfully created.' }
        format.json { render :show, status: :created, location: @mathima }
      else
        format.html { render :new }
        format.json { render json: @mathima.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /mathimas/1
  # PATCH/PUT /mathimas/1.json
  def update
    respond_to do |format|
      if @mathima.update(mathima_params)
        format.html { redirect_to @mathima, notice: 'Mathima was successfully updated.' }
        format.json { render :show, status: :ok, location: @mathima }
      else
        format.html { render :edit }
        format.json { render json: @mathima.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /mathimas/1
  # DELETE /mathimas/1.json
  def destroy
    @mathima.destroy
    respond_to do |format|
      format.html { redirect_to mathimas_url, notice: 'Mathima was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def mathima_params
      params.require(:mathima).permit(:mathima, :video_gallery_id,:taxi_id)
    end
end
