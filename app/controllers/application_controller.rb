class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :user_params, if: :devise_controller?

  def collect_all
    @mathimas = Mathima.all
    @taxis = Taxi.all
    @video_galleries = VideoGallery.all
  end

  rescue_from CanCan::AccessDenied do |exception|
    render :file => "#{Rails.root}/public/403.html", :status => 403, :layout => false
  end

  protected
    def user_params
      devise_parameter_sanitizer.permit(:sign_up,keys:[:email,:password,:password_confirmation,:onoma,:eponimo,:roles])
      devise_parameter_sanitizer.permit(:account_update,keys:[:email,:password,:password_confirmation,:onoma,:eponimo,:roles])
    end
end
