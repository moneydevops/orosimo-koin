class StaticPagesController < ApplicationController
  before_action :collect_all
  def home
  end

  def contact
  end

  def techno
  end
end
