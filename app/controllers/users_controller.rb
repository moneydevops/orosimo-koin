class UsersController < ApplicationController
  before_filter :collect_all
  before_filter :authenticate_user!
  before_filter :search
  load_and_authorize_resource

  def index
  end

  def show
  end

  def search
    if !params[:search].blank?
      @results = User.search(params[:search], :star=>true)
    end
  end

  def enable_users
    @user = User.find(params[:id])
    @user.approved = !@user.approved
    @user.save
    flash[:success] = @user.approved 
    redirect_to users_path
  end

  def change_user_role
    if @user.roles == 'user'
      @user.roles = 'admin'
      @user.save 
      flash[:success] = "user is now admin"
      redirect_to users_path
    else
      @user.roles = 'user'
      @user.save
      flash[:success] = "user is demoted to regular"
      redirect_to users_path
    end
  end
  def promote_to_teacher
    if @user.roles == 'user' || @user.roles == 'admin'
      @user.roles = 'teacher'
      @user.save
      flash[:success] = "user is now teacher"
      redirect_to users_path
    else
      @user.roles = 'user'
      @user.save
      flash[:success] = "user is now regular"
      redirect_to users_path
    end
  end

end

