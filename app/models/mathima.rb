class Mathima < ApplicationRecord
  has_many :video_galleries, dependent: :destroy
  has_many :suggested_posts
end
