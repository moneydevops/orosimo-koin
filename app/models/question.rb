class Question < ApplicationRecord
  has_many :answers 

  belongs_to :user
  belongs_to :video_gallery
  belongs_to :answer

  default_scope -> { order(created_at: :desc) }

  validates :body , presence: true, length: {maximum: 10000}
  validates :title , presence: true, length: {maximum: 150,minimum: 15}
  validates :user_id, presence: true
end
