class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  ROLES = %i[admin teacher user]

  has_many :questions, dependent: :destroy
  has_many :answers, dependent: :destroy
  has_many :question_comments

  after_save :populate_to_sphinx

  def admin?
    self.roles == "admin"
  end

  def teacher?
    self.roles == "teacher"
  end

  def active_for_authentication? 
    super && approved? 
  end 

  def inactive_message 
    if !approved? 
      :not_approved 
    else 
      super # Use whatever other message 
    end 
  end

  def populate_to_sphinx

        ThinkingSphinx::RealTime::Callbacks::RealTimeCallbacks.new(
              :user
                ).after_save self
  end
end
