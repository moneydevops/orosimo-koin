class Answer < ApplicationRecord
  has_many :questions
  has_many :question_comments
  belongs_to :question
  belongs_to :user
end
