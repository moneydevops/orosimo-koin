json.extract! question, :id, :body, :user_id, :video_gallery_id, :created_at, :updated_at
json.url question_url(question, format: :json)