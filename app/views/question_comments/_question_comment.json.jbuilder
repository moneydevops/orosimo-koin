json.extract! question_comment, :id, :question_id, :body, :created_at, :updated_at
json.url question_comment_url(question_comment, format: :json)
