json.extract! taxi, :id, :taxi, :created_at, :updated_at
json.url taxi_url(taxi, format: :json)