json.extract! mathima, :id, :mathima, :created_at, :updated_at
json.url mathima_url(mathima, format: :json)