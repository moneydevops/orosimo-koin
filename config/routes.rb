Rails.application.routes.draw do
  resources :question_comments
  mount Ckeditor::Engine => '/ckeditor'
  root 'static_pages#home'
  devise_for :users, controllers: {registrations: 'registrations', confirmations: 'confirmations'}
  resources :taxis,:mathimas
  resources :suggested_posts
  resources :video_galleries do
    put :order, on: :collection
  end
  get 'users/:user_id/questions/:question_id/answers', to: 'answers#index'
  resources :users , only: [:index, :show], :shallow=>true do
    resources :questions do
      get :profile_index, on: :collection
    end
    resources :answers
    member do
      patch :enable_users
      patch :change_user_role
      patch :promote_to_teacher
    end
    get :search, on: :collection
  end
  get 'contact'=> 'static_pages#contact'
  get 'technology'=> 'static_pages#techno'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
