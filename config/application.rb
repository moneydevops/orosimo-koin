require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

Dotenv::Railtie.load
module OrosimoKoin
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.autoload_paths += %W(#{config.root}/app/models/ckeditor)
    config.action_view.sanitized_allowed_tags = ['strong', 'em', 'a', 'body', 'table', 'td', 'href', 'a', 'tr', 'span', 'image','img','src','style','css','class','div','id','ul','il','&','tbody','br','p', 'h1','h2','h3','h4','scope']
    config.action_view.sanitized_allowed_attributes = ['href', 'title', 'strong', 'em', 'a', 'body', 'table', 'td', 'href', 'a', 'tr', 'span', 'image','img','src','style','css','class','div','id','ul','il','&','tbody','br','p', 'h1','h2','h3','scope']
  end
end
